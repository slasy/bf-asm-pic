		LIST p=16f84a

		#include "p16F84A.inc"
		#include <macros.inc>
		#include <shift_regs.inc>
		#include <i2c.inc>
		#include <brainfuck.inc>

		__CONFIG _FOSC_HS & _WDTE_OFF & _PWRTE_ON & _CP_OFF

		extern shift_out, shift_in
		extern I2C_HW_ADDR, I2C_SW_ADDR_L, I2C_SW_ADDR_H, I2C_DATA
		extern i2c_sram_write, i2c_sram_read, i2c_inc_sram_addr, i2c_dec_sram_addr
		extern i2c_eeprom_write, i2c_eeprom_read, i2c_inc_ee_addr, i2c_dec_ee_addr
		extern i2c_reset_ee_addr, i2c_reset_sram_addr
		extern reset_brainfuck, brainfuck

		global enter_wait

		udata_shr
TMP0	res		1
TMP1	res		1
TMP2	res		1

;začátek programu
RES_VECT		CODE
		goto	Init

;obsluha přerušení
INTERUPT		CODE
		retfie					;žádně přerušení, okamžitý návrat

MAIN_PROG		CODE
Init:
		set_bnk	1
		bcf		LED_I
		bcf		LED_B
		bsf		ENTER
		set_bnk	0

		bsf		LED_I
		bsf		LED_B

		init_sh_out
		movlw	0xFF
		call	shift_out
		init_sh_in
		init_i2c
		call	clear_ram
		movlw	0
		call	shift_out
		bcf		LED_I
		bcf		LED_B

Main:
		call	i2c_reset_sram_addr
		call	i2c_reset_ee_addr
		call	reset_brainfuck
		call	main_menu
		goto	Main

Quit:
		goto	Quit

OTHER	CODE

main_menu:
		bsf		LED_I
		call	enter_wait
		bcf		LED_I
		clrw
		call	shift_out
		bsf		LED_B
		call	shift_in
		andlw	b'00000011'
		movwf	TMP0
		bcf		LED_B
		addwf	PCL, F
		goto	menu0
		goto	menu1
		goto	menu2
		goto	menu3
menu0:
		call	brainfuck
		return
menu1:
		call	write_code
		return
menu2:
		call	clear_ram		;jen 256B
		return
menu3:
		call	clear_rom		;jen 256B
		return

write_code:
		movlf	BF_ROM_HW, I2C_HW_ADDR
		call	i2c_reset_ee_addr
_wc_loop:
		movfw	I2C_SW_ADDR_L
		andlw	b'00111111'
		addlw	b'01000000'
		call	shift_out
		bsf		LED_I
		call	enter_wait
		bcf		LED_I
		bsf		LED_B
		call	shift_in
		movwf	TMP0
		addlw	0xF8			;TMP>7?
		bc		_wc_quit
		movfw	TMP0
		call	i2c_eeprom_write
		call	i2c_inc_ee_addr
		bcf		LED_B
		goto	_wc_loop
_wc_quit:
		bcf		LED_B
		movlw	0x8
		call	i2c_eeprom_write
		call	i2c_reset_ee_addr
		clrw
		call	shift_out
		return

clear_ram:
		movlf	BF_RAM_HW, I2C_HW_ADDR
		bsf		LED_B
		call	i2c_reset_sram_addr
		clrf	TMP0
_clr_loop:
		clrw
		call	i2c_sram_write
		call	i2c_inc_sram_addr
		incfsz	TMP0, F
		goto	_clr_loop
		bcf		LED_B
		return

clear_rom:
		movlf	BF_ROM_HW, I2C_HW_ADDR
		bsf		LED_B
		call	i2c_reset_ee_addr
		clrf	TMP0
_clr_loop2:
		movlw	0xFF
		call	i2c_eeprom_write
		call	i2c_inc_ee_addr
		incfsz	TMP0, F
		goto	_clr_loop2
		bcf		LED_B
		return


delay:
;Delay 50000 cycles
        MOVLW 0x41  ;65 DEC
        MOVWF TMP1
        MOVLW 0x0EE  ;238 DEC
        MOVWF TMP0
        DECFSZ TMP0,F
        GOTO $-1
        DECFSZ TMP1,F
        GOTO $-3
		nop
		return

enter_wait:
		btfss	ENTER
		goto	enter_wait		; wait for "enter" key
		clrf	TMP0
input_wait1:
		incf	TMP0
		btfss	TMP0, 7
		goto	input_wait1		; short delay
input_wait2:
		btfsc	ENTER
		goto	input_wait2		; wait for "enter" key to release
		return

		END
