;=================
;Ovládání RTC DS1302
;=================
;je nutné nadefinovat
;<PORT>
;CE_DS
;IO_DS
;SCLK_DS
;<RAM>
;DS1302_ADDR
;TMP0
;TMP1

		#include <macros.inc>

		#ifndef __ds1302
		#define __ds1302

		#define	CE_DS	PORTB, 4
		#define IO_DS	PORTB, 3
		#define	SCLK_DS	PORTB, 2

init_ds1302		macro
		set_bnk	1
		bcf		IO_DS
		bcf		SCLK_DS
		bcf		CE_DS
		set_bnk 0
		bcf		IO_DS
		bcf		SCLK_DS
		bcf		CE_DS
		call	ds1302_WP_off
		call	ds1302_CH_off
		call	ds1302_WP_on
		endm

__ds1302_input	macro
		set_bnk	1
		bsf		IO_DS
		set_bnk 0
		endm

__ds1302_output	macro
		set_bnk	1
		bcf		IO_DS
		set_bnk 0
		endm

		#endif