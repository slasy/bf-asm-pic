		LIST p=16f84a

		#include "p16F84A.inc"
		#include <macros.inc>
		#include <shift_regs.inc>
		#include <i2c.inc>
		#include <ds1302.inc>
		#include <brainfuck.inc>

		__CONFIG _FOSC_HS & _WDTE_OFF & _PWRTE_ON & _CP_OFF		

		extern shift_out, shift_in
		extern I2C_HW_ADDR, I2C_SW_ADDR_L, I2C_SW_ADDR_H, I2C_DATA
		extern ds1302_sec, ds1302_min, ds1302_hour
		extern ds1302_WP_off, ds1302_CH_off, ds1302_WP_on
		extern DS1302_ADDR, DS1302_DATA
		extern i2c_sram_write, i2c_sram_read
		extern i2c_eeprom_write, i2c_eeprom_read, i2c_inc_ee_addr

sram_addr		equ		0xA0
eeprom_addr		equ		0xAE

		udata_shr
TMP0	res		1
TMP1	res		1
TMP2	res		1

;smazat?
testm	macro	hodnota
		nop
		endm

;začátek programu
RES_VECT		CODE
		goto	Init
		testm	"nejaka data"   ;smazat?

;obsluha přerušení
INTERUPT		CODE
		retfie					;žádně přerušení, okamžitý návrat

MAIN_PROG		CODE
Init:
		set_bnk	1
		movlw	0x00
		movwf	PORTB
		set_bnk	0

		init_sh_out
		init_sh_in
		init_i2c
		init_ds1302

;test SRAM
		movlf	sram_addr, I2C_HW_ADDR

		movlf	0x00, I2C_SW_ADDR_L
		movlw	'A'
		call	i2c_sram_write

		movlf	0x08, I2C_SW_ADDR_L
		movlw	'h'
		call	i2c_sram_write

		movlf	0x10, I2C_SW_ADDR_L
		movlw	'o'
		call	i2c_sram_write

		movlf	0x18, I2C_SW_ADDR_L
		movlw	'j'
		call	i2c_sram_write

;test EEPROM
		movlf	eeprom_addr, I2C_HW_ADDR

		movlf	0x00, I2C_SW_ADDR_L
		movlf	0x00, I2C_SW_ADDR_H
		movlw	'H'
		call	i2c_eeprom_write

		call	i2c_inc_ee_addr
		movlw	'e'
		call	i2c_eeprom_write

		call	i2c_inc_ee_addr
		movlw	'l'
		call	i2c_eeprom_write

		call	i2c_inc_ee_addr
		movlw	'l'
		call	i2c_eeprom_write

		call	i2c_inc_ee_addr
		movlw	'o'
		call	i2c_eeprom_write

		call	i2c_inc_ee_addr
		movlw	0x0A					;odřádkování
		call	i2c_eeprom_write

		call	i2c_inc_ee_addr
		movlw	0x0D
		call	i2c_eeprom_write

		movlf	sram_addr, I2C_HW_ADDR
		movlf	0, I2C_SW_ADDR_L
		call	i2c_sram_read
		call	shift_out

;Variables: TMP2, TMP1, TMP0
;Delay 5000001 cycles
        MOVLW 0x50  ;80 DEC
        MOVWF TMP2
        MOVLW 0x7E  ;126 DEC
        MOVWF TMP1
        MOVLW 0x0A4  ;164 DEC
        MOVWF TMP0
        DECFSZ TMP0,F
        GOTO $-1
        DECFSZ TMP1,F
        GOTO $-5
        DECFSZ TMP2,F
        GOTO $-9
;End of Delay

Main:
		call	shift_in
		call	shift_out
		goto	Main
Quit:
		goto	Quit

		END